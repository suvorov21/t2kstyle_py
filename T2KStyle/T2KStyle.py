import ROOT
from array import array

def GetT2K(WhichStyle=3, styleName="T2K"):
    t2kStyle=ROOT.TStyle(styleName, "T2K approved plots style")

    # -- WhichStyle --
    # 1 = presentation large fonts
    # 2 = presentation small fonts
    # 3 = publication/paper
    # 4 = publication/paper for jupyter
    explain = ("presentation large fonts",
               "presentation small fonts",
               "publication/paper")

    FontStyle = 22
    FontSizeLabel = 0.035
    FontSizeTitle = 0.05
    YOffsetTitle = 1.3

    if WhichStyle == 1:
        FontStyle = 42
        FontSizeLabel = 0.05
        FontSizeTitle = 0.065
        YOffsetTitle = 1.3
    elif WhichStyle == 2:
        FontStyle = 42
        FontSizeLabel = 0.035
        FontSizeTitle = 0.05
        YOffsetTitle = 1.6
    elif WhichStyle == 3:
        FontStyle = 132
        FontSizeLabel = 0.05#35
        FontSizeTitle = 0.071#5
        YOffsetTitle = 1.1#1.1

    # use plain black on white colors
    t2kStyle.SetFrameBorderMode(0)
    t2kStyle.SetCanvasBorderMode(0)
    t2kStyle.SetPadBorderMode(0)
    t2kStyle.SetCanvasBorderSize(0)
    t2kStyle.SetFrameBorderSize(0)
    t2kStyle.SetDrawBorder(0)
    t2kStyle.SetTitleBorderSize(0)

    t2kStyle.SetPadColor(0)
    t2kStyle.SetCanvasColor(0)
    t2kStyle.SetStatColor(0)
    t2kStyle.SetFillColor(0)

    t2kStyle.SetEndErrorSize(4)
    t2kStyle.SetStripDecimals(ROOT.kFALSE)

    t2kStyle.SetLegendBorderSize(0)
    t2kStyle.SetLegendFont(FontStyle)

    # set the paper & margin sizes
    t2kStyle.SetPaperSize(20, 26)
    t2kStyle.SetPadTopMargin(0.1)
    t2kStyle.SetPadBottomMargin(0.15)# 0.15 in official
    t2kStyle.SetPadRightMargin(0.15) # 0.075 -> 0.13 for colz option
    t2kStyle.SetPadLeftMargin(0.16)#to include both large/small font options

    # Fonts, sizes, offsets
    t2kStyle.SetTextFont(FontStyle)
    t2kStyle.SetTextSize(0.10)#08

    t2kStyle.SetLabelFont(FontStyle, "x")
    t2kStyle.SetLabelFont(FontStyle, "y")
    t2kStyle.SetLabelFont(FontStyle, "z")
    t2kStyle.SetLabelFont(FontStyle, "t")
    t2kStyle.SetLabelSize(FontSizeLabel, "x")
    t2kStyle.SetLabelSize(FontSizeLabel, "y")
    t2kStyle.SetLabelSize(FontSizeLabel, "z")
    t2kStyle.SetLabelOffset(0.015, "x")
    t2kStyle.SetLabelOffset(0.015, "y")
    t2kStyle.SetLabelOffset(0.015, "z")

    t2kStyle.SetTitleFont(FontStyle, "x")
    t2kStyle.SetTitleFont(FontStyle, "y")
    t2kStyle.SetTitleFont(FontStyle, "z")
    t2kStyle.SetTitleFont(FontStyle, "t")
    t2kStyle.SetTitleSize(FontSizeTitle, "t")
    t2kStyle.SetTitleSize(FontSizeTitle, "y")
    t2kStyle.SetTitleSize(FontSizeTitle, "x")
    t2kStyle.SetTitleSize(FontSizeTitle, "z")
    t2kStyle.SetTitleOffset(0.9, "x") #1.2 nominally
    t2kStyle.SetTitleOffset(YOffsetTitle, "y")
    t2kStyle.SetTitleOffset(1.2, "z")

    t2kStyle.SetTitleStyle(0)
    t2kStyle.SetTitleFontSize(0.06)#0.08
    t2kStyle.SetTitleFont(FontStyle, "pad")
    t2kStyle.SetTitleBorderSize(0)
    t2kStyle.SetTitleX(0.1)#5
    t2kStyle.SetTitleW(0.8)

    # use bold lines and markers
    t2kStyle.SetMarkerStyle(20)
    t2kStyle.SetHistLineWidth(2)
    t2kStyle.SetLineStyleString(2, "[12 12]") # prescript dashes

    # get rid of X error bars and y error bar caps
#    t2kStyle.SetErrorX(0.001)

    # do not display any of the standard histogram decorations
    t2kStyle.SetOptTitle(1)
    t2kStyle.SetOptStat(0)
    t2kStyle.SetOptFit(0)

    # put tick marks on top and RHS of plots
    t2kStyle.SetPadTickX(1)
    t2kStyle.SetPadTickY(1)

    # -- color --
    # functions blue
    t2kStyle.SetFuncColor(600-4)

    t2kStyle.SetFillColor(1) # make color fillings (not white)
    # - color setup for 2D -
    # - "cold"/ blue-ish -
    red = array('d',[0.00, 0.09, 0.18, 0.09, 0.00])
    green = array('d',[0.01, 0.02, 0.39, 0.68, 0.97])
    blue  = array('d',[0.17, 0.39, 0.62, 0.79, 0.97])
    # - "warm" red-ish colors -
    #  red = array('d',[1.00, 1.00, 0.25])
    #  green = array('d',[1.00, 0.00, 0.00])
    #  blue = array('d',[0.00, 0.00, 0.00])

    stops = array('d',[0.00, 0.02, 0.20, 0.60, 1.00])
    NRGBs = 5
    NCont = 255

    ROOT.TColor.CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont)
    t2kStyle.SetNumberContours(NCont)

    # - Rainbow -
    #  t2kStyle.SetPalette(1)  # use the rainbow color set

    # -- axis --
    t2kStyle.SetStripDecimals(ROOT.kFALSE) # don't do 1.0 -> 1
    #  ROOT.TGaxis.SetMaxDigits(3) # doesn't have an effect
    # no supressed zeroes!
    t2kStyle.SetHistMinimumZero(ROOT.kTRUE)

    print(f'Style for {explain[WhichStyle-1]}')

    return t2kStyle

def SetT2KStyle(WhichStyle=3):
    t2kStyle = GetT2K(WhichStyle)
    ROOT.gROOT.SetStyle("T2K")
    ROOT.gROOT.ForceStyle()

SetT2KStyle(3)
