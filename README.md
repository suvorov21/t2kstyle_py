# T2K style
T2K official style for ROOT plots with python. The style is optimized for 800x600 pads.

## Installation
As a usual pip package in the dev mode
```bash
python3 -m pip install -e .
```

## Usage
```python
import ROOT
from T2KStyle.T2KStyle import GetT2K
style = GetT2K(3)
ROOT.gROOT.SetStyle("T2K")
```
should give you
```
Welcome to JupyROOT 6.24/00
Style for publication/paper
```

You can change the style between
1. presentation large fonts
2. presentation small fonts
3. publication/paper

The desired integer should be set as an argument for `GetT2K()`

To work with files, the style should be set before opening a TFile

