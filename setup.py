from setuptools import setup

setup(name='T2KStyle',
      version='0.1',
      description='The T2K style for ROOT',
      url='http://github.com/storborg/funniest',
      author='Sergey',
      license='GPLv3',
      packages=setuptools.find_packages(),
      python_requires='>=3.6'
      )